//Classe respons�vel por estabelecer a comunica��o com o servidor para atualiza��o dos dados do ve�culo
package Controllers;

import java.io.ByteArrayOutputStream;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import unb.telemetriaveicular.DAO.JsonHandler;
import unb.telemetriaveicular.dbhelper.CarHelper;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Updater {

	private Context context;

	public Updater(Context context) {
		this.context = context;
	}

	public boolean isConnected() {
		ConnectivityManager connMgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			return true;
		}
		return false;
	}

	public String httpRequest(String uri) {
		// if (!isConnected()) {
		// return "sem conexao";
		// }
		HttpClient httpclient = new DefaultHttpClient();
		String responseString = null;
		try {
			HttpResponse response = httpclient.execute(new HttpGet(uri));
			StatusLine statusLine = response.getStatusLine();

			if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				responseString = out.toString();
			} else {
				response.getEntity().getContent().close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseString;
	}

	public void updateDatabaseFromJson(String json) {
		context.deleteDatabase(CarHelper.DB_NAME);
		JsonHandler.jsonToDb(json, context);
	}

}
