//Classe respons�vel pela obten��o dos dados do ve�culo cadastrado atrav�s de uma chamada para o servidor
package Controllers;

import java.util.ArrayList;
import java.util.List;

import unb.telemetriaveicular.R;
import unb.telemetriaveicular.DAO.DAOCar;
import unb.telemetriaveicular.DAO.DAOSimpleCar;
import unb.telemetriaveicular.model.SimpleCar;
import android.content.Context;

public class ListController {

	private List<SimpleCar> simpleCars = new ArrayList<SimpleCar>();
	private List<Boolean> updatedPlates = new ArrayList<Boolean>();

	private Context context;
	private DAOSimpleCar dao;

	private static ListController instance;

	private ListController(Context context) {
		this.context = context;
		dao = new DAOSimpleCar(context);
	}

	public static ListController getInstance(Context context) {
		if (instance == null)
			instance = new ListController(context);
		return instance;
	}

	public void updatePlates() {
		dao.open();
		simpleCars = dao.getAllSimpleCars();
		dao.close();
	}

	public List<SimpleCar> getSimpleCars() {
		return simpleCars;
	}

	public void deleteSelectedPlates(List<Boolean> selectedItems) {

		DAOCar daocar = new DAOCar(context);
		daocar.open();
		dao.open();
		for (int i = 0; i < selectedItems.size(); i++) {
			if (selectedItems.get(i)) {
				dao.deleteSimpleCar(simpleCars.get(i).getPlate());
				daocar.deleteCar(simpleCars.get(i).getPlate());
			}
		}
		dao.close();
		daocar.close();
	}

	public String buildRequestString() {
		// Model: ["22JJJ1111","KKK2221","KKK2222"]

		List<SimpleCar> simplecars = getSimpleCars();
		StringBuilder request = new StringBuilder();
		request.append("[%22");
		for (SimpleCar string : simplecars) {
			request.append(string.getPlate());
			request.append("%22,%22");
		}

		if (request.toString().contains("%22,%22"))
			request.delete(request.lastIndexOf("%22,%22"), request.length());

		request.append("%22]");
		String requestString = context.getString(R.string.RequestURL)
				+ request.toString();

		return requestString;
	}

	public void setPlates(List<SimpleCar> plates) {
		this.simpleCars = plates;
	}

	public void addOnList(SimpleCar simpleCar) {
		dao.open();
		dao.insertSimpleCar(simpleCar);
		dao.close();
	}

	public void checkUpdatedPlates() {
		DAOCar daoCar = new DAOCar(context);
		daoCar.open();
		updatedPlates.clear();
		for (SimpleCar simpleCar : simpleCars) {
			if (daoCar.get(simpleCar.getPlate(), null) == null)
				updatedPlates.add(false);
			else
				updatedPlates.add(true);
		}
		daoCar.close();
	}

	public List<Boolean> getUpdatedPlates() {
		return updatedPlates;
	}

	public void setUpdatedPlates(List<Boolean> updatedPlates) {
		this.updatedPlates = updatedPlates;
	}
}
