package unb.telemetriaveicular.dbhelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CarHelper extends SQLiteOpenHelper {

	public static final String TABLE_CARS = "cars";
	public static final String COLUMN_PLATE = "plate_id";
	public static final String COLUMN_BRAND = "brand";
	public static final String COLUMN_VEHICLE = "vehicle";
	
	public static final String COLUMN_IDEAL_PRESSURE_FRONT = "ideal_pressure_front";
	public static final String COLUMN_IDEAL_PRESSURE_BACK = "ideal_pressure_back";
	public static final String COLUMN_MAX_DISPLACEMENT = "max_displacement";
	
	public static final int DB_VERSION = 1;
	public static final String DB_NAME = "cars.db";
	
	private Context context;

	public CarHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		this.context=context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table " + 
					TABLE_CARS + "(" + COLUMN_PLATE + " PRIMARY KEY"+
					            ", " + COLUMN_BRAND +
					            ", " + COLUMN_VEHICLE +
					            ", " + COLUMN_IDEAL_PRESSURE_FRONT +
					            ", " + COLUMN_IDEAL_PRESSURE_BACK +
					            ", " + COLUMN_MAX_DISPLACEMENT +");");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		context.deleteDatabase(DB_NAME);
		onCreate(db);
	}

}
