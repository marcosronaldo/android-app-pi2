package unb.telemetriaveicular.dbhelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SimpleCarHelper extends SQLiteOpenHelper {

	public static final String TABLE_SIMPLE_CAR = "plates";
	public static final String COLUMN_NICKNAME = "nickname";
	public static final String COLUMN_PLATE = "plate_id";

	public static final int DB_VERSION = 1;
	public static final String DB_NAME = "plates.db";

	private Context context;

	public SimpleCarHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table " + TABLE_SIMPLE_CAR + "(" + COLUMN_PLATE
				+ " PRIMARY KEY" + ", " + COLUMN_NICKNAME + ");");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		context.deleteDatabase(DB_NAME);
		onCreate(db);
	}

}
