package unb.telemetriaveicular.GUI;

import java.util.ArrayList;
import java.util.List;

import unb.telemetriaveicular.R;
import unb.telemetriaveicular.model.SimpleCar;
import Controllers.ListController;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

public class PlateListAdapter extends ArrayAdapter<SimpleCar> {

	private final Context context;
	private final List<SimpleCar> values;
	private final List<Boolean> selectedItems;
	private final List<Boolean> updatedItems;

	public List<Boolean> getSelectedItems() {
		return selectedItems;
	}

	public PlateListAdapter(Context context, List<SimpleCar> values) {
		super(context, R.layout.item_list, values);
		ListController.getInstance(context).checkUpdatedPlates();
		
		this.context = context;
		this.values = values;
		selectedItems = new ArrayList<Boolean>();
		updatedItems = new ArrayList<Boolean>();
		for (int i = 0; i < values.size(); i++) {
			selectedItems.add(false);
		}
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.item_list, parent, false);
		
		TextView textView = (TextView) rowView.findViewById(R.id.firstLine);
		textView.setText(values.get(position).getNickName());
		if(!ListController.getInstance(context).getUpdatedPlates().get(position))
			textView.setTextColor(Color.RED);
		
		textView = (TextView) rowView.findViewById(R.id.secondLine);
		textView.setText(values.get(position).getPlate());

		CheckBox check = (CheckBox) rowView.findViewById(R.id.checkbox);
		if (rowView != null) {
			check.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					CheckBox cb = (CheckBox) v;
					selectedItems.set(position, cb.isChecked());
				}
			});
		}

		return rowView;
	}

}