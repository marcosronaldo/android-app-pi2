package unb.telemetriaveicular.GUI;

import unb.telemetriaveicular.R;
import unb.telemetriaveicular.DAO.DAOSimpleCar;
import unb.telemetriaveicular.model.SimpleCar;
import Controllers.ListController;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class ActivityVehicleAdd extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vehicle_add);
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.vehicle_add, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void addVehicle(View view) {
		EditText editText = (EditText) findViewById(R.id.edit_vehicle_plate);
		String plate = editText.getText().toString();
		
		editText = (EditText) findViewById(R.id.edit_vehicle_nickname);
		String nickname = editText.getText().toString();
		
		SimpleCar sc = new SimpleCar();
		sc.setNickName(nickname);
		sc.setPlate(plate);
		
		ListController.getInstance(this).addOnList(sc);	
		NavUtils.navigateUpTo(this, new Intent(this, ActivityVechicleList.class));
	}

}
