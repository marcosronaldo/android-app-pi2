package unb.telemetriaveicular.GUI.Tab;

import java.util.Arrays;

import unb.telemetriaveicular.R;
import unb.telemetriaveicular.DAO.DAOCar;
import unb.telemetriaveicular.model.Car;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.androidplot.xy.XYStepMode;

public class FragmentTires extends Fragment {

	 public static FragmentTires newInstance(String plate) {
		 FragmentTires f = new FragmentTires();
	        Bundle args = new Bundle();
	        args.putString("plate", plate);
	        f.setArguments(args);
	        return f;
	    }
	
	private Car car;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.tire_chart,
				container, false);

		String stringPlate = getArguments().getString("plate");

		if (stringPlate != null) {
			DAOCar dao = new DAOCar(getActivity());
			dao.open();
			car = dao.get(stringPlate, null);
			dao.close();
		}

		setGraphic(R.id.mySimpleXYPlot, rootView, "Dianteiro Esquerdo",1);
		setGraphic(R.id.mySimpleXYPlot2, rootView, "Dianteiro Direito",2);
		setGraphic(R.id.mySimpleXYPlot3, rootView, "Traseiro Esquerdo",3);
		setGraphic(R.id.mySimpleXYPlot4, rootView, "Traseiro Direito",4);

		return rootView;
	}

	private void setGraphic(int id, View rootView, String title, int tire) {
		XYPlot plot = (XYPlot) rootView.findViewById(id);
		XPlotFormat format = new XPlotFormat();
		format.setLabels(car.getTiresStates(),tire);
		
		Number[] values=new Number[format.Labels.size()];

		for(int i=0;i<format.numbers.size();i++)
			values[i]= format.numbers.get(i);
		
		XYSeries series1 = new SimpleXYSeries(Arrays.asList(values),
				SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "");

		LineAndPointFormatter series1Format = new LineAndPointFormatter(
				Color.BLUE, Color.RED, null, null);
		
		plot.setRangeBoundaries(0,60, BoundaryMode.FIXED);
		
		plot.setDomainStep(XYStepMode.SUBDIVIDE, format.Labels.size());
		
		plot.getGraphWidget().setDomainValueFormat(format);
		
		plot.setTitle(title);
		plot.addSeries(series1, series1Format);
		plot.setTicksPerRangeLabel(3);
		plot.getGraphWidget().setDomainLabelOrientation(-45);
	}
}