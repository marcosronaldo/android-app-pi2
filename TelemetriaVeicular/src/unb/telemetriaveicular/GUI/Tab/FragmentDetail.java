//Classe que gera a tab para apresentar os detalhes do ve�culo selecionado para visualizar os dados
package unb.telemetriaveicular.GUI.Tab;

import unb.telemetriaveicular.R;
import unb.telemetriaveicular.DAO.DAOCar;
import unb.telemetriaveicular.model.Car;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FragmentDetail extends Fragment {

	private Car car = new Car();

	 public static FragmentDetail newInstance(String plate) {
	        FragmentDetail f = new FragmentDetail();
	        Bundle args = new Bundle();
	        args.putString("plate", plate);
	        f.setArguments(args);
	        return f;
	    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_details, container,
				false);
		String stringPlate = getArguments().getString("plate");
		
		if (stringPlate!= null) {
			DAOCar dao = new DAOCar(getActivity());
			dao.open();
			car = dao.get(stringPlate, null);
			dao.close();
		}
		
		TextView name = (TextView) rootView
				.findViewById(R.id.vehicleNameTextViewValue);
		TextView brand = (TextView) rootView
				.findViewById(R.id.vehicleBrandTextViewValue);
		TextView plate = (TextView) rootView
				.findViewById(R.id.vehiclePlateTextViewValue);
		TextView pneuDianteiroEsquerdo = (TextView) rootView
				.findViewById(R.id.vehicleFrontLeftPressureViewValue);
		TextView pneuDianteiroDireito = (TextView) rootView
				.findViewById(R.id.vehicleFrontRightPressureViewValue);
		TextView pneuTraseiroEsquerdo = (TextView) rootView
				.findViewById(R.id.vehicleBackLeftPressureViewValue);
		TextView pneuTraseiroDireito = (TextView) rootView
				.findViewById(R.id.vehicleBackRightPressureViewValue);
		TextView pressaoDianteiraIdeal = (TextView) rootView
				.findViewById(R.id.vehicleIdealPressureFrontViewValue);
		TextView pressaoTraseiraIdeal = (TextView) rootView
				.findViewById(R.id.vehicleIdealPressureBackViewValue);

		if (car != null) {
			name.setText(name.getText() + " " + car.getVehicle());
			brand.setText(brand.getText() + " " + car.getBrand());
			plate.setText(plate.getText() + " " + car.getPlate());
			pneuDianteiroEsquerdo.setText (String.valueOf(car.getTiresStates().get(car.getTiresStates().size()-1).getTireOnePressure()));
			pneuDianteiroDireito.setText (String.valueOf(car.getTiresStates().get(car.getTiresStates().size()-1).getTireTwoPressure()));
			pneuTraseiroEsquerdo.setText (String.valueOf(car.getTiresStates().get(car.getTiresStates().size()-1).getTireThreePressure()));
			pneuTraseiroDireito.setText (String.valueOf(car.getTiresStates().get(car.getTiresStates().size()-1).getTireFourPressure()));
			pressaoDianteiraIdeal.setText(pressaoDianteiraIdeal.getText() + " " + String.valueOf(car.getIdealPressureTireFront()));
			pressaoTraseiraIdeal.setText(pressaoTraseiraIdeal.getText() + " " + String.valueOf(car.getIdealPressureTireBack()));
			
			pneuDianteiroEsquerdo.setTextColor(getColour(car.getTiresStates().get(car.getTiresStates().size()-1).getTireOnePressure(), 1));
			pneuDianteiroDireito.setTextColor(getColour(car.getTiresStates().get(car.getTiresStates().size()-1).getTireTwoPressure(),1));
			pneuTraseiroEsquerdo.setTextColor(getColour(car.getTiresStates().get(car.getTiresStates().size()-1).getTireThreePressure(),2));
			pneuTraseiroDireito.setTextColor(getColour(car.getTiresStates().get(car.getTiresStates().size()-1).getTireFourPressure(), 2));

		}
		
		return rootView;
	}
	
	private int getColour (double value, int type){
		if (type == 1){
			if (value > car.getIdealPressureTireFront()+3)
				return Color.RED;
		
			else if (car.getIdealPressureTireFront()-3.0 <= value && value <= car.getIdealPressureTireFront()+3.0)
				return Color.GREEN;
			
			else if (car.getIdealPressureTireFront()-7<= value && value <  car.getIdealPressureTireFront()-3)
				return Color.parseColor("#f6a615");
			else if (value <car.getIdealPressureTireFront()-7 && value >= car.getIdealPressureTireFront()-15)
				return Color.RED;
			else if (value <car.getIdealPressureTireFront()-15)
				return Color.parseColor("#2ae8db");
		}
		else {
			if (value > car.getIdealPressureTireBack()+3)
				return Color.RED;
		
			else if (car.getIdealPressureTireBack()-3.0 <= value && value <= car.getIdealPressureTireBack()+3.0)
				return Color.GREEN;
			
			else if (car.getIdealPressureTireBack()-7<= value && value <  car.getIdealPressureTireBack()-3)
				return Color.parseColor("#f6a615");
			else if (value <car.getIdealPressureTireBack()-7 && value >= car.getIdealPressureTireBack()-15)
				return Color.RED;
			else if (value <car.getIdealPressureTireBack()-15)
				return Color.parseColor("#2ae8db");
		}
		return Color.RED;
	}
	
}