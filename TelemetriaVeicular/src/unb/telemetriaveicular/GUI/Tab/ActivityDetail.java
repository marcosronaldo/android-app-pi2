//Classe principal das tabs para demonstra��o dos dados do ve�culo selecionado
package unb.telemetriaveicular.GUI.Tab;

import unb.telemetriaveicular.R;
import unb.telemetriaveicular.DAO.DAOSimpleCar;
import unb.telemetriaveicular.model.SimpleCar;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

public class ActivityDetail extends FragmentActivity implements
		ActionBar.TabListener {

	private ViewPager viewPager;
	private TabsAdapter mAdapter;
	private ActionBar actionBar;
	private String[] tabs = { "Detalhes", "Pneus"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vehicle_details);

		Intent intent = getIntent();
		String stringPlate = intent.getStringExtra("plate");

		viewPager = (ViewPager) findViewById(R.id.pager);

		actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		mAdapter = new TabsAdapter(getSupportFragmentManager(), stringPlate);

		DAOSimpleCar dao = new DAOSimpleCar(this);
		dao.open();
		String nickname = dao.getNickname(stringPlate);
		dao.close();
		
		this.setTitle("Carro: " + nickname);
		
		viewPager.setAdapter(mAdapter);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Adding Tabs
		for (String tab_name : tabs) {
			actionBar.addTab(actionBar.newTab().setText(tab_name)
					.setTabListener(this));
		}

		/**
		 * on swiping the viewpager make respective tab selected
		 * */
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// change fragment when tab selected
		viewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	}
}