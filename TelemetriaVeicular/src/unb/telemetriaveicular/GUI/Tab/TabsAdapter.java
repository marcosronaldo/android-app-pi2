package unb.telemetriaveicular.GUI.Tab;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
 
public class TabsAdapter extends FragmentPagerAdapter {
 
	private String plate;
	
    public TabsAdapter(FragmentManager fm, String plate) {
        super(fm);
        this.plate = plate;
    }
 
    @Override
    public Fragment getItem(int index) {
        switch (index) {
        case 0:
            return FragmentDetail.newInstance(plate);
        case 1:
            return FragmentTires.newInstance(plate);
        //case 2:
          //  return new FragmentSuspension();
        }
        return null;
    }
 
    @Override
    public int getCount() {
        //equal to number of tabs
        return 2;
    }
 
}