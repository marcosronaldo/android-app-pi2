//Classe que gera a Tab dos gr�ficos utilizados para apresentar os dados de press�o do pneu com o passar do tempo
package unb.telemetriaveicular.GUI.Tab;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import unb.telemetriaveicular.model.TiresState;

public class XPlotFormat extends Format {

	public ArrayList<String> Labels = new ArrayList<String>();
	public ArrayList<Number> numbers = new ArrayList<Number>();
	
	public void setLabels(List<TiresState> states, int tire){
		ArrayList<Date> dates = new ArrayList<Date>();		
		for (int i=0;i<states.size();i++){
			Date date = states.get(i).getDate();
			dates.add(date);
		}
	
		Collections.reverse(dates);
		Date lastDate=dates.get(0);
		Labels.add(lastDate.toLocaleString().substring(0, 5));
		
		numbers.add(states.get(states.size()-1).getTirePressure(tire));
		
		for(int i=1;i<dates.size();i++){
			Date date=dates.get(i);
			
			if(lastDate.getDay()!=date.getDay() || lastDate.getMonth()!=date.getMonth() || lastDate.getYear()!=date.getYear()){
				lastDate=date;
				Labels.add(lastDate.toLocaleString().substring(0, 5));
				numbers.add(states.get(states.size()-1-i).getTirePressure(tire));
			}	
			if(Labels.size()>20)
				break;
		}
		Collections.reverse(Labels);
		Collections.reverse(numbers);
	}

	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo,
			FieldPosition pos) {

		double i = ((Number) obj).floatValue();
		int index = (int)Math.round(i);
		
		if (Labels == null || Labels.size() <= index)
			return new StringBuffer("");

		return new StringBuffer(Labels.get(index));
	}

	@Override
	public Object parseObject(String string, ParsePosition position) {
		// TODO Auto-generated method stub
		return null;
	}

}
