package unb.telemetriaveicular.GUI;

import java.util.List;

import unb.telemetriaveicular.R;
import unb.telemetriaveicular.GUI.Tab.ActivityDetail;
import Controllers.ListController;
import Controllers.Updater;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class ActivityVechicleList extends ListActivity {

	private PlateListAdapter adapter;
	MenuItem refreshItem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vechicle_list);
		setAdapter();
	}

	private void setAdapter() {
		ListController.getInstance(this).updatePlates();
		adapter = new PlateListAdapter(this, ListController.getInstance(this)
				.getSimpleCars());
		setListAdapter(adapter);
	}

	@Override
	public void onListItemClick(ListView listView, View view, int position,
			long id) {
		super.onListItemClick(listView, view, position, id);

		if (ListController.getInstance(this).getUpdatedPlates().get(position)) {
			Intent detailIntent = new Intent(this, ActivityDetail.class);
			detailIntent.putExtra("plate", ListController.getInstance(this)
					.getSimpleCars().get(position).getPlate());
			startActivity(detailIntent);
		} else {
			int duration = Toast.LENGTH_LONG;
			Toast toast = Toast.makeText(this, "N�o h� dados deste ve�culo.",
					duration);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_new:
			vehicleAddActivity();
			return true;
		case R.id.action_remove:
			removeVehicle();
			return true;
		case R.id.action_refresh:
			new UpdateData(this).execute(ListController.getInstance(this)
					.buildRequestString());
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void removeVehicle() {
		final List<Boolean> selectedItems = adapter.getSelectedItems();
		AlertDialog.Builder adb = new AlertDialog.Builder(this);

		if (selectedItems.size() > 0) {
			adb.setTitle(getString(R.string.action_remove));
			adb.setMessage(getString(R.string.message_delete));
			adb.setNegativeButton("Cancelar", null);
			adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					ListController.getInstance(getApplicationContext())
							.deleteSelectedPlates(selectedItems);
					recreate();
				}
			});
			adb.show();
		} else {
			adb.setTitle(getString(R.string.title_error));
			adb.setMessage(getString(R.string.error_message_delete));
			adb.setPositiveButton("Ok", null);
			adb.show();
		}
	}

	public void vehicleAddActivity() {
		Intent detailIntent = new Intent(this, ActivityVehicleAdd.class);
		startActivity(detailIntent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.vehicle_list, menu);

		refreshItem = (menu.findItem(R.id.action_refresh));
		return super.onCreateOptionsMenu(menu);
	}

	private class UpdateData extends AsyncTask<String, String, String> {
		private Updater updater;
		private Context context;

		public UpdateData(Context context) {
			this.context = context;
			updater = new Updater(context);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			refresh();

		}

		public void refresh() {
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			ImageView iv = (ImageView) inflater.inflate(R.layout.refresh, null);

			Animation rotation = AnimationUtils.loadAnimation(context,
					R.animator.rotate);
			rotation.setRepeatCount(Animation.INFINITE);
			iv.startAnimation(rotation);

			refreshItem.setActionView(iv);
		}

		@Override
		protected String doInBackground(String... uri) {
			if (!updater.isConnected())
				return "Sem conex�o de dados.";
			String result = updater.httpRequest(uri[0]);
			updater.updateDatabaseFromJson(result);
			return "ok";
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (!result.equals("ok")) {
				int duration = Toast.LENGTH_LONG;
				Toast toast = Toast.makeText(context, result, duration);
				toast.show();
			}else{
				setAdapter();
			}
			
			refreshItem.getActionView().clearAnimation();
			refreshItem.setActionView(null);

		}
	}

}