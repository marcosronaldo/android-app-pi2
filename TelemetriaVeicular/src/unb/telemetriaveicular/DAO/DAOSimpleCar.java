/* Classe responsável pela comunicação com o banco de dados para manter dados da lista de carros 
cadastrados para acompanhamento*/
package unb.telemetriaveicular.DAO;

import java.util.ArrayList;
import java.util.List;

import unb.telemetriaveicular.dbhelper.SimpleCarHelper;
import unb.telemetriaveicular.model.SimpleCar;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class DAOSimpleCar {

	private SimpleCarHelper helper;
	private SQLiteDatabase db;
	private String[] allColumns = { SimpleCarHelper.COLUMN_PLATE,
			SimpleCarHelper.COLUMN_NICKNAME };

	public DAOSimpleCar(Context context) {
		helper = new SimpleCarHelper(context);
	}

	/**
	 * Opens the database
	 * 
	 * @throws SQLException
	 */
	public void open() throws SQLException {
		db = helper.getWritableDatabase();
	}

	/**
	 * Closes the carHelper
	 */
	public void close() {
		db.close();
		helper.close();
	}

	public void insertSimpleCar(SimpleCar simpleCar) throws SQLiteException {
		ContentValues values = new ContentValues();
		values.put(SimpleCarHelper.COLUMN_PLATE, simpleCar.getPlate());
		values.put(SimpleCarHelper.COLUMN_NICKNAME, simpleCar.getNickName());

		db.insert(SimpleCarHelper.TABLE_SIMPLE_CAR, null, values);
	}

	public void deleteSimpleCar(String plate) {
		db.delete(SimpleCarHelper.TABLE_SIMPLE_CAR,
				SimpleCarHelper.COLUMN_PLATE + " = '" + plate + "'", null);
	}

	public List<SimpleCar> getAllSimpleCars() {
		Cursor cursor = db.query(SimpleCarHelper.TABLE_SIMPLE_CAR, allColumns,
				null, null, null, null, null);

		List<SimpleCar> list = cursorToSimpleCars(cursor);
		cursor.close();
		return list;
	}

	public List<SimpleCar> cursorToSimpleCars(Cursor cursor) {
		List<SimpleCar> simpleCars = new ArrayList<SimpleCar>();
		if (!cursor.moveToFirst())
			return simpleCars;

		while (!cursor.isAfterLast()) {
			SimpleCar sc = new SimpleCar();

			sc.setPlate(cursor.getString(0));
			sc.setNickName(cursor.getString(1));
			simpleCars.add(sc);
			cursor.moveToNext();
		}
		return simpleCars;
	}

	public String getNickname(String plate) {
		String nickname;

		Cursor cursor = db.query(SimpleCarHelper.TABLE_SIMPLE_CAR, allColumns,
				SimpleCarHelper.COLUMN_PLATE + " = '" + plate + "'", null,
				null, null, null);

		if (!cursor.moveToFirst())
			return null;

		nickname = cursor.getString(1);

		cursor.close();
		return nickname;
	}

	public boolean simpleCarExists(String plate) {
		Cursor cursor = db.query(SimpleCarHelper.TABLE_SIMPLE_CAR, allColumns,
				SimpleCarHelper.COLUMN_PLATE + " = '" + plate + "'", null,
				null, null, null);
		if (!cursor.moveToFirst())
			return false;
		return true;
	}
}
