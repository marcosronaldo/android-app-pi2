package unb.telemetriaveicular.DAO;

import unb.telemetriaveicular.model.Car;
import unb.telemetriaveicular.model.Cars;
import android.content.Context;

import com.google.gson.Gson;

public class JsonHandler {
	public static Gson gson = new Gson();

	public static void jsonToDb(String json, Context context) {
		DAOCar dao = new DAOCar(context);
		dao.open();
		Cars cars = jsonToModel(json);

		for (Car car : cars.getCars()) {
			dao.insertCar(car);
		}
		dao.close();
	}

	private static Cars jsonToModel(String json) {
		Cars cars = new Cars();
		cars = gson.fromJson(json, Cars.class);
		return cars;
	}

}
