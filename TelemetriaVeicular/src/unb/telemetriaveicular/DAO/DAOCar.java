/*Classe respons�vel pela comunica��o com o banco de dados para manter os dados espec�ficos dos ve�culos
cadastrados para acompanhamento*/
package unb.telemetriaveicular.DAO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import unb.telemetriaveicular.dbhelper.CarHelper;
import unb.telemetriaveicular.model.Car;
import unb.telemetriaveicular.model.SuspensionsState;
import unb.telemetriaveicular.model.TiresState;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class DAOCar {

	public static final String SCOLUMN_ID = "_id";
	public static final String SCOLUMN_BODY_ONE = "bodyOne";
	public static final String SCOLUMN_TIRE_ONE = "tireOne";
	public static final String SCOLUMN_BODY_TWO = "bodyTwo";
	public static final String SCOLUMN_TIRE_TWO = "tireTwo";
	public static final String SCOLUMN_BODY_THREE = "bodyThree";
	public static final String SCOLUMN_TIRE_THREE = "tireThree";
	public static final String SCOLUMN_BODY_FOUR = "bodyFour";
	public static final String SCOLUMN_TIRE_FOUR = "tireFour";
	public static final String SCOLUMN_DATE = "date";

	public static final String TCOLUMN_ID = "_id";
	public static final String TCOLUMN_TIRE_ONE = "tireOne";
	public static final String TCOLUMN_TIRE_TWO = "tireTwo";
	public static final String TCOLUMN_TIRE_THREE = "tireThree";
	public static final String TCOLUMN_TIRE_FOUR = "tireFour";
	public static final String TCOLUMN_DATE = "date";
	private CarHelper carHelper;
	private SQLiteDatabase db;

	private String[] allColumns = { CarHelper.COLUMN_BRAND,
			CarHelper.COLUMN_PLATE, CarHelper.COLUMN_VEHICLE,
			CarHelper.COLUMN_IDEAL_PRESSURE_FRONT,
			CarHelper.COLUMN_IDEAL_PRESSURE_BACK,
			CarHelper.COLUMN_MAX_DISPLACEMENT };

	private String[] allColumnsSuspension = { SCOLUMN_ID, SCOLUMN_BODY_ONE,
			SCOLUMN_TIRE_ONE, SCOLUMN_BODY_TWO, SCOLUMN_TIRE_TWO,
			SCOLUMN_BODY_THREE, SCOLUMN_TIRE_THREE, SCOLUMN_BODY_FOUR,
			SCOLUMN_TIRE_FOUR, SCOLUMN_DATE };

	private String[] allColumnsTire = { TCOLUMN_ID, TCOLUMN_TIRE_ONE,
			TCOLUMN_TIRE_TWO, TCOLUMN_TIRE_THREE, TCOLUMN_TIRE_FOUR,
			TCOLUMN_DATE };

	public DAOCar(Context context) {
		carHelper = new CarHelper(context);
	}

	/**
	 * Opens the database
	 * 
	 * @throws SQLException
	 */
	public void open() throws SQLException {
		db = carHelper.getWritableDatabase();
	}

	/**
	 * Closes the carHelper
	 */
	public void close() {
		db.close();
		carHelper.close();
	}

	/**
	 * @param car
	 * @throws SQLiteException
	 */
	public void insertCar(Car car) throws SQLiteException {

		ContentValues values = new ContentValues();
		values.put(CarHelper.COLUMN_PLATE, car.getPlate());
		values.put(CarHelper.COLUMN_BRAND, car.getBrand());
		values.put(CarHelper.COLUMN_VEHICLE, car.getVehicle());
		values.put(CarHelper.COLUMN_IDEAL_PRESSURE_FRONT,
				car.getIdealPressureTireFront());
		values.put(CarHelper.COLUMN_IDEAL_PRESSURE_BACK,
				car.getIdealPressureTireBack());
		values.put(CarHelper.COLUMN_MAX_DISPLACEMENT,
				car.getSpringMaxDisplacement());

		if (get(car.getPlate(), null) != null){
			update(car,values);
		}else{
		
		db.insert(CarHelper.TABLE_CARS, null, values);

		db.execSQL(getCreateTableSuspensionScript(car.getPlate()
				+ "_SUSPENSION"));
		db.execSQL(getCreateTableTireScript(car.getPlate() + "_TIRE"));
		}
		insertSuspensionsStates(car);
		insertTiresStates(car);
	}
	
	public void update(Car car,ContentValues values){
		db.update(CarHelper.TABLE_CARS, values, CarHelper.COLUMN_PLATE + " = '" + car.getPlate()
				+ "'", null);
	}

	/**
	 * Insert the tires states of a car in the database
	 * 
	 * @param car
	 */
	private void insertTiresStates(Car car) {
		for (TiresState t : car.getTiresStates()) {
			ContentValues v = new ContentValues();

			v.put(TCOLUMN_TIRE_ONE, t.getTireOnePressure());
			v.put(TCOLUMN_TIRE_TWO, t.getTireTwoPressure());
			v.put(TCOLUMN_TIRE_THREE, t.getTireThreePressure());
			v.put(TCOLUMN_TIRE_FOUR, t.getTireFourPressure());
			v.put(TCOLUMN_DATE, t.getDate().getTime());
			db.insert(car.getPlate() + "_TIRE", null, v);
		}
	}

	/**
	 * Insert the suspensions states of a car in the database
	 * 
	 * @param car
	 */
	private void insertSuspensionsStates(Car car) {
		for (SuspensionsState s : car.getSuspensionsStates()) {
			ContentValues v = new ContentValues();
			v.put(SCOLUMN_BODY_ONE, s.getSuspensionOneBody());
			v.put(SCOLUMN_BODY_TWO, s.getSuspensionTwoBody());
			v.put(SCOLUMN_BODY_THREE, s.getSuspensionThreeBody());
			v.put(SCOLUMN_BODY_FOUR, s.getSuspensionFourBody());

			v.put(SCOLUMN_TIRE_ONE, s.getSuspensionOneTire());
			v.put(SCOLUMN_TIRE_TWO, s.getSuspensionTwoTire());
			v.put(SCOLUMN_TIRE_THREE, s.getSuspensionThreeTire());
			v.put(SCOLUMN_TIRE_FOUR, s.getSuspensionFourTire());

			v.put(SCOLUMN_DATE, s.getDate().getTime());
			db.insert(car.getPlate() + "_SUSPENSION", null, v);
		}
	}

	/**
	 * Deletes a car in the database, droping suspension and tire tables.
	 * 
	 * @param car
	 */
	public void deleteCar(String plate) {
		db.delete(CarHelper.TABLE_CARS, CarHelper.COLUMN_PLATE + " = '" + plate
				+ "'", null);
		db.execSQL("DROP TABLE IF EXISTS " + plate + "_SUSPENSION");
		db.execSQL("DROP TABLE IF EXISTS " + plate + "_TIRE");
	}

	/**
	 * @param minDate
	 * @return list with all cars in the database, with the data from minDate
	 */
	public List<Car> getAllCars(Date minDate) {
		List<Car> cars = new ArrayList<Car>();

		Cursor cursor = db.query(CarHelper.TABLE_CARS, allColumns, null, null,
				null, null, null);

		if (!cursor.moveToFirst())
			return cars;

		while (!cursor.isAfterLast()) {
			Car car = cursorToCar(cursor);
			car.setSuspensionsStates(getSuspensionsStates(car, minDate));
			car.setTiresStates(getTiresState(car, minDate));
			cars.add(car);
			cursor.moveToNext();
		}
		cursor.close();
		return cars;
	}

	/**
	 * @param plate
	 * @param minDate
	 * @return a car with plate = input parameter "plate", with all the date
	 *         alter minDate
	 */
	public Car get(String plate, Date minDate) {
		Car car = null;
		Cursor cursor = db.query(CarHelper.TABLE_CARS, allColumns,
				CarHelper.COLUMN_PLATE + " = '" + plate + "'", null, null,
				null, null);
		cursor.moveToFirst();
		car = cursorToCar(cursor);
		cursor.close();

		if (car != null) {
			car.setSuspensionsStates(getSuspensionsStates(car, minDate));
			car.setTiresStates(getTiresState(car, minDate));
		}
		return car;
	}

	/**
	 * @param car
	 * @param minDate
	 * @return List with all SuspensionsStates after minDate date.
	 */
	public List<SuspensionsState> getSuspensionsStates(Car car, Date minDate) {
		Cursor cursor = db.query(car.getPlate() + "_SUSPENSION",
				allColumnsSuspension, null, null, null, null, "date");
		if (!cursor.moveToFirst())
			return null;
		ArrayList<SuspensionsState> states = new ArrayList<SuspensionsState>();

		while (!cursor.isAfterLast()) {
			SuspensionsState state = cursorToSuspensionsState(cursor);
			if (minDate == null || state.getDate().after(minDate))
				states.add(state);
			cursor.moveToNext();
		}

		cursor.close();
		return states;
	}

	/**
	 * @param car
	 * @param minDate
	 * @return List with all tireStates after minDate date.
	 */
	public List<TiresState> getTiresState(Car car, Date minDate) {
		Cursor cursor = db.query(car.getPlate() + "_TIRE", allColumnsTire,
				null, null, null, null, "date");
		if (!cursor.moveToFirst())
			return null;
		ArrayList<TiresState> states = new ArrayList<TiresState>();

		while (!cursor.isAfterLast()) {
			TiresState state = cursorToTiresState(cursor);
			if (minDate == null || state.getDate().after(minDate))
				states.add(state);
			cursor.moveToNext();
		}
		cursor.close();
		return states;
	}

	/**
	 * @param cursor
	 * @return a car which the cursor is pointing with the basic information
	 */
	private Car cursorToCar(Cursor cursor) {
		if (cursor == null || cursor.isAfterLast())
			return null;

		Car car = new Car();
		car.setBrand(cursor.getString(0));
		car.setPlate(cursor.getString(1));
		car.setVehicle(cursor.getString(2));
		car.setIdealPressureTireFront(cursor.getInt(3));
		car.setIdealPressureTireBack(cursor.getInt(4));
		car.setSpringMaxDisplacement(cursor.getInt(5));

		return car;
	}

	/**
	 * @param cursor
	 * @return SuspensionsState one state of the suspension
	 */
	private SuspensionsState cursorToSuspensionsState(Cursor cursor) {
		SuspensionsState state = new SuspensionsState();

		state.setSuspensionOneTire(cursor.getDouble(1));
		state.setSuspensionOneBody(cursor.getDouble(2));
		state.setSuspensionTwoTire(cursor.getDouble(3));
		state.setSuspensionTwoBody(cursor.getDouble(4));
		state.setSuspensionThreeTire(cursor.getDouble(5));
		state.setSuspensionThreeBody(cursor.getDouble(6));
		state.setSuspensionFourTire(cursor.getDouble(7));
		state.setSuspensionFourBody(cursor.getDouble(8));
		state.setDate(new Date(cursor.getLong(9)));

		return state;
	}

	/**
	 * @param cursor
	 * @return TiresState one state of the suspension
	 */
	private TiresState cursorToTiresState(Cursor cursor) {
		TiresState state = new TiresState();

		state.setTireOnePressure(cursor.getDouble(1));
		state.setTireTwoPressure(cursor.getDouble(2));
		state.setTireThreePressure(cursor.getDouble(3));
		state.setTireFourPressure(cursor.getDouble(4));

		state.setDate(new Date(cursor.getLong(5)));

		return state;
	}

	/**
	 * @param name
	 * @return String with the full create table script
	 */
	private String getCreateTableSuspensionScript(String name) {
		return "create table " + name + "(" + SCOLUMN_ID
				+ " integer primary key autoincrement" + ", "
				+ SCOLUMN_BODY_ONE + ", " + SCOLUMN_TIRE_ONE + ", "
				+ SCOLUMN_BODY_TWO + ", " + SCOLUMN_TIRE_TWO + ", "
				+ SCOLUMN_BODY_THREE + ", " + SCOLUMN_TIRE_THREE + ", "
				+ SCOLUMN_BODY_FOUR + ", " + SCOLUMN_TIRE_FOUR + ", "
				+ SCOLUMN_DATE + ");";
	}

	/**
	 * @param name
	 * @return String with the full create table script
	 */
	private String getCreateTableTireScript(String name) {
		return "create table " + name + "(" + TCOLUMN_ID
				+ " integer primary key autoincrement" + ", "
				+ TCOLUMN_TIRE_ONE + ", " + TCOLUMN_TIRE_TWO + ", "
				+ TCOLUMN_TIRE_THREE + ", " + TCOLUMN_TIRE_FOUR + ", "
				+ TCOLUMN_DATE + ");";
	}

}