// Classe modelo para instancia��o de dados de suspens�o do ve�culo
package unb.telemetriaveicular.model;

import java.util.Date;

public class SuspensionsState {
	double suspensionOneBody;
	double suspensionOneTire;

	double suspensionTwoBody;
	double suspensionTwoTire;

	double suspensionThreeBody;
	double suspensionThreeTire;

	double suspensionFourBody;
	double suspensionFourTire;

	Date date;

	public SuspensionsState() {
		suspensionFourBody = 0;
		suspensionFourTire = 0;
		suspensionOneBody = 0;
		suspensionOneTire = 0;
		suspensionThreeBody = 0;
		suspensionThreeTire = 0;
		suspensionTwoBody = 0;
		suspensionTwoTire = 0;
		date = new Date();
	}

	public SuspensionsState(double suspensionOneBody, double suspensionOneTire,
			double suspensionTwoBody, double suspensionTwoTire,
			double suspensionThreeBody, double suspensionThreeTire,
			double suspensionFourBody, double suspensionFourTire, Date date) {
		super();
		this.suspensionOneBody = suspensionOneBody;
		this.suspensionOneTire = suspensionOneTire;
		this.suspensionTwoBody = suspensionTwoBody;
		this.suspensionTwoTire = suspensionTwoTire;
		this.suspensionThreeBody = suspensionThreeBody;
		this.suspensionThreeTire = suspensionThreeTire;
		this.suspensionFourBody = suspensionFourBody;
		this.suspensionFourTire = suspensionFourTire;
		this.date = date;
	}

	public double getSuspensionOneBody() {
		return suspensionOneBody;
	}

	public void setSuspensionOneBody(double suspensionOneBody) {
		this.suspensionOneBody = suspensionOneBody;
	}

	public double getSuspensionOneTire() {
		return suspensionOneTire;
	}

	public void setSuspensionOneTire(double suspensionOneTire) {
		this.suspensionOneTire = suspensionOneTire;
	}

	public double getSuspensionTwoBody() {
		return suspensionTwoBody;
	}

	public void setSuspensionTwoBody(double suspensionTwoBody) {
		this.suspensionTwoBody = suspensionTwoBody;
	}

	public double getSuspensionTwoTire() {
		return suspensionTwoTire;
	}

	public void setSuspensionTwoTire(double suspensionTwoTire) {
		this.suspensionTwoTire = suspensionTwoTire;
	}

	public double getSuspensionThreeBody() {
		return suspensionThreeBody;
	}

	public void setSuspensionThreeBody(double suspensionThreeBody) {
		this.suspensionThreeBody = suspensionThreeBody;
	}

	public double getSuspensionThreeTire() {
		return suspensionThreeTire;
	}

	public void setSuspensionThreeTire(double suspensionThreeTire) {
		this.suspensionThreeTire = suspensionThreeTire;
	}

	public double getSuspensionFourBody() {
		return suspensionFourBody;
	}

	public void setSuspensionFourBody(double suspensionFourBody) {
		this.suspensionFourBody = suspensionFourBody;
	}

	public double getSuspensionFourTire() {
		return suspensionFourTire;
	}

	public void setSuspensionFourTire(double suspensionFourTire) {
		this.suspensionFourTire = suspensionFourTire;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "SuspensionsState [suspensionOneBody=" + suspensionOneBody
				+ ", suspensionOneTire=" + suspensionOneTire
				+ ", suspensionTwoBody=" + suspensionTwoBody
				+ ", suspensionTwoTire=" + suspensionTwoTire
				+ ", suspensionThreeBody=" + suspensionThreeBody
				+ ", suspensionThreeTire=" + suspensionThreeTire
				+ ", suspensionFourBody=" + suspensionFourBody
				+ ", suspensionFourTire=" + suspensionFourTire + ", date="
				+ date + "]";
	}
	
	
}
