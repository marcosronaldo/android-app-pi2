//Classe modelo para a instancia��o de estados de press�o dos pneus do ve�culo
package unb.telemetriaveicular.model;

import java.util.Date;

public class TiresState {
	double tireOnePressure;
	double tireTwoPressure;
	double tireThreePressure;
	double tireFourPressure;

	Date date;

	public TiresState() {
		tireFourPressure = 0;
		tireOnePressure = 0;
		tireThreePressure = 0;
		tireTwoPressure = 0;
		date=new Date();
	}

	public TiresState(double tireOnePressure, double tireTwoPressure,
			double tireThreePressure, double tireFourPressure, Date date) {
		super();
		this.tireOnePressure = tireOnePressure;
		this.tireTwoPressure = tireTwoPressure;
		this.tireThreePressure = tireThreePressure;
		this.tireFourPressure = tireFourPressure;
		this.date = date;
	}
	
	
	public double getTirePressure(int tire){
		switch(tire){
		case 1:
			return getTireOnePressure();
		case 2: 
			return getTireTwoPressure();
		case 3:
			return getTireThreePressure();
		case 4:
			return getTireFourPressure();
		default:
			return 0.0;
		}
	}

	public double getTireOnePressure() {
		return tireOnePressure;
	}

	public void setTireOnePressure(double tireOnePressure) {
		this.tireOnePressure = tireOnePressure;
	}

	public double getTireTwoPressure() {
		return tireTwoPressure;
	}

	public void setTireTwoPressure(double tireTwoPressure) {
		this.tireTwoPressure = tireTwoPressure;
	}

	public double getTireThreePressure() {
		return tireThreePressure;
	}

	public void setTireThreePressure(double tireThreePressure) {
		this.tireThreePressure = tireThreePressure;
	}

	public double getTireFourPressure() {
		return tireFourPressure;
	}

	public void setTireFourPressure(double tireFourPressure) {
		this.tireFourPressure = tireFourPressure;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "TiresState [tireOnePressure=" + tireOnePressure
				+ ", tireTwoPressure=" + tireTwoPressure
				+ ", tireThreePressure=" + tireThreePressure
				+ ", tireFourPressure=" + tireFourPressure + ", date=" + date
				+ "]";
	}
	
	

}
