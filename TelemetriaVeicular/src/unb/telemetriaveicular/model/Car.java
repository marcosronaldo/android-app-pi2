//Classe modelo para inst�ncia de carros cadastrados 
package unb.telemetriaveicular.model;

import java.util.ArrayList;
import java.util.List;

public class Car {

	private String vehicle;
	private String brand;
	private String plate;

	private int idealPressureTireFront;
	private int idealPressureTireBack;
	private int springMaxDisplacement;

	//lista de estados de suspens�o e pneu do carro
	List<SuspensionsState> suspensionsStates;
	List<TiresState> tiresStates;

	public Car() {
		this.vehicle = "";
		this.brand = "";
		this.plate = "";
		this.idealPressureTireBack = 0;
		this.idealPressureTireFront = 0;
		this.springMaxDisplacement = 0;
		this.suspensionsStates = new ArrayList<SuspensionsState>();
		this.tiresStates = new ArrayList<TiresState>();
	}

	public Car(String vehicle, String brand, String plate,
			int idealPressureTireFront, int idealPressureTireBack,
			int springMaxDisplacement) {
		super();
		this.vehicle = vehicle;
		this.brand = brand;
		this.plate = plate;
		this.idealPressureTireFront = idealPressureTireFront;
		this.idealPressureTireBack = idealPressureTireBack;
		this.springMaxDisplacement = springMaxDisplacement;
		this.suspensionsStates = new ArrayList<SuspensionsState>();
		this.tiresStates = new ArrayList<TiresState>();
	}

	public String getVehicle() {
		return vehicle;
	}

	public void setVehicle(String vehicle) {
		this.vehicle = vehicle;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getPlate() {
		return plate;
	}

	public void setPlate(String plate) {
		this.plate = plate;
	}

	public List<SuspensionsState> getSuspensionsStates() {
		return suspensionsStates;
	}

	public void setSuspensionsStates(List<SuspensionsState> suspensionsStates) {
		this.suspensionsStates = suspensionsStates;
	}

	public List<TiresState> getTiresStates() {
		return tiresStates;
	}

	public void setTiresStates(List<TiresState> tiresStates) {
		this.tiresStates = tiresStates;
	}

	public void addSuspensionState(SuspensionsState s) {
		this.suspensionsStates.add(s);
	}

	public void addTireState(TiresState t) {
		this.tiresStates.add(t);
	}

	public int getIdealPressureTireFront() {
		return idealPressureTireFront;
	}

	public void setIdealPressureTireFront(int idealPressureTireFront) {
		this.idealPressureTireFront = idealPressureTireFront;
	}

	public int getIdealPressureTireBack() {
		return idealPressureTireBack;
	}

	public void setIdealPressureTireBack(int idealPressureTireBack) {
		this.idealPressureTireBack = idealPressureTireBack;
	}

	public int getSpringMaxDisplacement() {
		return springMaxDisplacement;
	}

	public void setSpringMaxDisplacement(int springMaxDisplacement) {
		this.springMaxDisplacement = springMaxDisplacement;
	}

	@Override
	public String toString() {
		return "Car [vehicle=" + vehicle + ", brand=" + brand + ", plate="
				+ plate + ", idealPressureTireFront=" + idealPressureTireFront
				+ ", idealPressureTireBack=" + idealPressureTireBack
				+ ", springMaxDisplacement=" + springMaxDisplacement
				+ ", suspensionsStates=" + suspensionsStates + ", tiresStates="
				+ tiresStates + "]";
	}

}
