//Classe que armazena um conjunto de carros
package unb.telemetriaveicular.model;

import java.util.ArrayList;
import java.util.List;

public class Cars {

	List<Car> cars;
	
	public Cars(){
		this.cars = new ArrayList<Car>(); 
	}

	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}
	
	public void addCar(Car car){
		cars.add(car);
	}
	
	public void removeCar(Car car){
		cars.remove(car);
	}
	
	
}
