//Classe modelo para a instancia��o de carros que o usu�rio deseja monitorar
package unb.telemetriaveicular.model;

public class SimpleCar {

	private String plate;
	private String nickName;
	
	public String getPlate() {
		return plate;
	}
	public void setPlate(String plate) {
		this.plate = plate;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
}